function gettingAllDataForComp(inputData , companyName , callbacks){

    const allData = {}
    allData[companyName] = inputData.reduce((result , currKey)=>{
        if(currKey['company'] === companyName){
            result.push(currKey)
        }
        return result
    },[])

    callbacks(null , allData)
}

module.exports = gettingAllDataForComp