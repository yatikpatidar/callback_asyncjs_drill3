function swapPosition(inputData , id1 , id2 , callback){

    const copyData = JSON.parse(JSON.stringify(inputData))
    
    let index1 = -1 ;
    let index2 = -1
    
    copyData.forEach((element , index)=>{
        if(element['id'] === id1 ){
            index1 = index
        }else if(element['id'] === id2){
            index2 = index
        }
    })
    
    const temp = copyData[index1]
    copyData[index1] = copyData[index2] 
    copyData[index2] = temp

    callback(null , copyData)

}

module.exports = swapPosition