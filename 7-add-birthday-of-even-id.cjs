function addBirthday(inputData , callback){
    const resultList = inputData.reduce((res , currKey)=>{
        if(currKey['id']%2===0){
            const date= new Date()
            const dateString = `${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`
            currKey['birthday'] = dateString
        }
        res.push(currKey)
        return res
        
    },[])

    callback(null , resultList)
}
module.exports = addBirthday