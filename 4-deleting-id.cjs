function deletingId(inputData , id , callback){

    const afterDeleting = inputData.reduce((result , currKey)=>{

        if(currKey['id'] != id){
            result.push(currKey)    
        }
        return result

    } ,[]) 

    callback(null , afterDeleting)

}

module.exports = deletingId