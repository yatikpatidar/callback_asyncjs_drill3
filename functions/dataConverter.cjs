
const fs = require('fs')
const data = require('../data/1-employees-callbacks.cjs')

function dataWriting(data) {
    try {

        fs.writeFile('./data/data.json', JSON.stringify(data, null,), { encoding: 'utf8' }, (err, callbackData) => {
            if (err) {
                throw new Error(err)
            } else {
                console.log("data written successfully !")
            }
        })
    }
    catch(err){
        console.error(err)
    }
}

dataWriting(data["employees"])