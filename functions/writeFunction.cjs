const fs = require('fs')
function writeFileFun(filePath, data, callback) {

    fs.writeFile(filePath, JSON.stringify(data, null, 2), (err) => {
        try {
            // console.log("inside try writefile")
            callback(err)
        } catch (err) {            
            console.log(`error in file ${filePath} while writing file`)
        }
    })
}

module.exports =writeFileFun