function retrieveDataFun(inputData , ids ,callbacks){

    const idsData = inputData.reduce((res , currKey)=>{

        if(ids.includes(currKey['id'])){
            res.push(currKey)
        }
        return res

    },[])

    callbacks(null , idsData)
}

module.exports = retrieveDataFun