function sortDataByComp(inputData , callbacks){

    const sortedData = JSON.parse(JSON.stringify(inputData))
     sortedData.sort((a,b)=>{

        if(a['company']=== b['company']){
            return a['id']-b['id']
        }
        else if(a['company'] > b['company']){
            return 1
        }
        else{
            return -1
        }
    })
    
    callbacks(null , sortedData)
}

module.exports = sortDataByComp