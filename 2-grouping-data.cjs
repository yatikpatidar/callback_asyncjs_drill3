
function groupDataByCompany(inputData , callbacks){

    const groupList = inputData.reduce((result , currKey)=>{
        
        const companyName = currKey['company']

        if(result[companyName] === undefined){
            result[companyName] = []
            result[companyName].push(currKey)
        }
        else{
            result[companyName].push(currKey)
        }

        return result
    } ,{})

    callbacks(null , groupList)
}

module.exports = groupDataByCompany