const writeFileFunction = require('./../functions/writeFunction.cjs')
const inputData = require('./../data/data.json')

const retrieveDataFun = require('./../1-retrieve-data.cjs')
const groupDataByCompany = require('./../2-grouping-data.cjs')
const gettingAllDataForComp = require('./../3-getting-all-data.cjs')
const deletingId = require('./../4-deleting-id.cjs')
const sortDataByComp = require('./../5-sort-data-based-on-company.cjs')
const swapPosition = require('./../6-swap-position-of-companies.cjs')
const addBirthday = require('./../7-add-birthday-of-even-id.cjs')

// 1. Retrieve data for ids : [2, 13, 23]
const ids = [2, 13, 23]
retrieveDataFun(inputData, ids, (retrieveCallbackErr, retrieveCallbackData) => {
    try {

        if (retrieveCallbackErr) {
            throw new Error(err)
        } else {
            // console.log(retrieveCallbackData)
            writeFileFunction('./output/1outputRetrieveData.json', retrieveCallbackData, (err) => {

                if (err) {
                    console.log(writeFileErr)
                    return

                } else {

                    // 2. Group data based on companies.
                    // { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
                    groupDataByCompany(inputData, (groupCallbackErr, groupCallbackData) => {

                        if (groupCallbackErr) {
                            throw new Error(err)
                        }
                        else {

                            // console.log(groupCallbackData)
                            writeFileFunction('./output/2outputGroupByCompany.json', groupCallbackData, (err) => {
                                if (err) {
                                    console.log(err)
                                    return
                                }

                                // 3. Get all data for company Powerpuff Brigade
                                const compName = 'Powerpuff Brigade'
                                gettingAllDataForComp(inputData, compName, (allDataCallbackErr, allDataCallbackData) => {

                                    if (allDataCallbackErr) {
                                        throw new Error(allDataCallbackErr)
                                    }
                                    else {

                                        // console.log(allDataCallbackData)
                                        writeFileFunction('./output/3outputGettingAllData.json', allDataCallbackData, (err) => {
                                            if (err) {
                                                console.log(err)
                                                return
                                            }
                                            
                                            // 4. Remove entry with id 2.
                                            const id = 2
                                            deletingId(inputData, id, (deletingCallbackErr, deletingCallbackData) => {
                                                if (deletingCallbackErr) {
                                                    console.log("error in deleting id callback", deletingCallbackErr)
                                                    return
                                                } else {
                                                    // console.log(deletingCallbackData)
                                                    writeFileFunction('./output/4deletingId.json', deletingCallbackData, (err) => {
                                                        if (err) {
                                                            console.log(err)
                                                            return
                                                        }

                                                        // 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
                                                        sortDataByComp(inputData, (sortCallbackErr, sortCallbackData) => {
                                                            if (sortCallbackErr) {
                                                                throw new Error(sortCallbackErr)
                                                            }
                                                            // console.log(sortCallbackData)
                                                            writeFileFunction('./output/5outputSortedData.json', sortCallbackData, (err) => {
                                                                if (err) {
                                                                    console.log(err)
                                                                    return
                                                                }
                                                                
                                                                // 6. Swap position of companies with id 93 and id 92.
                                                                const id1 = 92
                                                                const id2 = 93
                                                                swapPosition(inputData ,id1 , id2 , (swapCallbackErr , swapCallbackData)=>{
                                                                    if(swapCallbackErr){
                                                                        throw new Error(swapCallbackErr)
                                                                    }
                                                                    
                                                                    // console.log(swapCallbackData)
                                                                    writeFileFunction('./output/6outputSwapPosition.json' , swapCallbackData , (err)=>{
                                                                        if(err){
                                                                            console.log(err)
                                                                            return
                                                                        }

                                                                        // 7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`. 
                                                                        addBirthday(inputData , (birthdayCallbackErr , birthdayCallbackData)=>{
                                                                            if(birthdayCallbackErr){
                                                                                throw new Error(birthdayCallbackErr)
                                                                            }
                                                                            // console.log(birthdayCallbackData)

                                                                            writeFileFunction('./output/7outputAddBirthday.json' , birthdayCallbackData,(err)=>{
                                                                                if(err){
                                                                                    console.log(err)
                                                                                    return 
                                                                                }
                                                                            })
                                                                        })
                                                                    })
                                                                } )
                                                            })
                                                        })
                                                    })
                                                }
                                            })
                                        })
                                    }
                                })
                            })
                        }
                    })
                }
            })
        }
    } catch (err) {
        console.log(err)
    }
})
